perlutils
=========
2015 Brian Eldrige

Collection of public perl utilities for use with various third party PCB tools

Usage
-----------
### Environment Setup (Windows)
The following installations are recommended for use under Windows.

|Tool          | Notes                                        | Install Example / Link                                  |
|:-------------|:---------------------------------------------|:--------------------------------------------------------|
| [Perl-win]   | [Perl] IDE & Strawberry Perl Install         | Follow link and instructions                            |
|              |                                              | Include instructions for App::cpanminus install as well |
| [Git-win]    | [Git] Version Control System for Windows     | Link will auto-download, run installer                  |
|              |                                              |   Choose all defaults except select this:               |
|              |                                              |   *-> Use Git from Windows Cmd Prompt*                  |
| [Emacs-win]  | [Emacs] for windows (optional)               | Download latest emacs-XX.X-bin.zip, unzip               |


### Workspace Setup
Once the above are downloaded, the following must be done to ensure everything is configured correctly.  Note that
these commands may be prevented from behind corporate firewalls and may need to be executed from an open connection:

```sh
From a Windows Command prompt:
$ cd c:\                                                 # 
$ git clone https://bitbucket.org/eldridge/perlutils.git # pull the latest perl utils down from git
$ cd perlutils                                           # enter the perlutils dir
$ cpanm --installdeps .                                  # Install all perl dependencies
```

Update your windows "$PATH" Environment Variable to include "c:\perlutils"

* You should now be set up and ready to develop or use the tools!


[Perl-win]:http://learn.perl.org/installing/windows.html
[Git-win]:http://git-scm.com/download/win
[Emacs-win]:ftp://ftp.gnu.org/gnu/emacs/windows/
[Perl]:http://www.perl.org/
[Git]:http://git-scm.com/
[Emacs]:https://www.gnu.org/software/emacs/
[cpanminus]:http://search.cpan.org/~miyagawa/App-cpanminus-1.7016/lib/App/cpanminus.pm
