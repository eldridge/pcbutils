################################################################################
#
# A Perl Module with commonly used utilities
#
################################################################################

use strict;
use warnings;

package Util;

require Exporter;
our @ISA       = qw(Exporter);
our @EXPORT_OK = qw($Script $Verb $Quiet Dump Debug Info Error slurp slurpr spew spewon);

use CGI::Carp;
use Time::CTime;
use File::Slurp;
use File::Basename;
use Data::Dumper;
use File::Path;
use DateTime;

our $Script = basename($0);
our $Verb   = 0;
our $Quiet  = 0;

################################################################################
# Function to slurp an entire file using File:: but with verbosity option
################################################################################
sub slurp {
    my $file = shift;

    my @lines = (-d $file) ? read_dir($file) : read_file($file);
    my $numl  = @lines;
    
    print "$Script: INFO: $numl lines read from $file\n" if ($Verb>1);
    return wantarray ? @lines : "@lines";
};

################################################################################
# Function to recursively slurp an entire file with INCLUDE statements
################################################################################
sub slurpr {
    my $file = shift;
    my @IN;
    for (slurp($file)) {
        push (@IN,(/^INCLUDE,\s*([^,\s]+)/) ? slurpr($1) : $_);
    };
    return wantarray ? @IN : "@IN";
};

################################################################################
# Function to write an entire file using File:: but with verbosity option
################################################################################
sub spew {
    my $file = shift;
    my @data = @_;

    my $dir  = dirname($file);
    my $numl = @data;

    mkpath($dir,$Verb,0755);

    my $ok   = write_file($file, @data);

    print "$Script: INFO: $numl lines written to $file\n" if $ok and ($Verb>1);
}

################################################################################
# Function to append to a file using File:: but with verbosity option
################################################################################
sub spewon {
    my $file = shift;
    my @data = @_;

    my $numl = @data;
    my $ok   = write_file($file, {append => 1}, @data);

    print "$Script: INFO: $numl lines appended to $file\n" if $ok and ($Verb>1);
}

################################################################################
# Dump out variables using Data::Dumper
################################################################################
sub Dump {
    my $level = shift;
    my $name  = shift;
    if ($level <= $Verb) {print Data::Dumper->Dump([shift],[$name])}
}


################################################################################
# Print out Debug messages based on verbosity settings
################################################################################
sub Debug {
    my $level = shift;
    my @msg   = @_;

    if ($level <= $Verb) {print "$_\n" for (@msg)};
}


################################################################################
# Print out Info messages based on verbosity settings
################################################################################
sub Info {
    my $level = shift;
    my $msg   = shift;

    if ($level <= $Verb) {print "$Script: INFO: $msg\n"};;
}


################################################################################
# Print out Error message and exit
################################################################################
sub Error {
    my $msg = shift; 

    croak( "ERROR: $msg" );
}


################################################################################
# The End
################################################################################
1
