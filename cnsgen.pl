#!/usr/local/bin/perl -w
##############################################################################################
# File          : cnsgen.pl
# Author        : Brian Eldridge
# Version       : 1.0
# Created       : 2015/04/07
###############################################################################################
# Description   : Parses and generates Mentor CES compatible CSV files
#               : 
#
###############################################################################################

=head1 NAME

csngen.pl - Parses and generates Mentor CES compatible CSV files

=head1 SYNOPSIS

cnsgen.pl [options]

  Command line options:
  -h[elp]              :  (print usage message and exit)
  -v[erbose]           :  (enable debug verbosity)
  -v[erbose]=N         :  (enable MORE verbosity)
  -V[erbose]           :  (shorthand for -v=2)
  -i[nput]=<filename>  :  (reads constraings from <filename>)

=cut

################################################################################
# Beginning of Program
################################################################################
use strict;
use warnings;

BEGIN { unshift(@INC, ".") }

use Pod::Usage;
use Getopt::Long;
use Text::CSV;

use Util qw($Script $Verb $Quiet Debug Error Info Dump slurp spew);

################################################################################
# Globals and Command Line Options
################################################################################
our %CES;

my $help = 0;
my $csvfile;

$Getopt::Long::ignorecase = 0;
GetOptions( "help"      => \$help,
            "verbose:i" => sub {$Verb = ($_[1]) ? $_[1] : 1},
            "Verbose"   => sub {$Verb = 2},
            "input=s"   => \$csvfile,
            )
    || Error("Unrecognized command line option(s)");

################################################################################
# Usage message:
################################################################################
if (!$csvfile)  {$help = 1};
if ($help == 1) { pod2usage(); };

################################################################################
# The Guts
################################################################################

process_ces_file($csvfile);


################################################################################
# The End
################################################################################
exit 1;

################################################################################
# Read a decrypted csv file in Mentor CES format
################################################################################
sub process_ces_file {

    my $file = shift;
    my @CES = slurp($file);
    chomp @CES;
    
    my $csv = Text::CSV->new({sep_char=>';'});

    # parse the full CES
    while (@CES) {

        # Ignore leading blank lines
        if ($CES[0] =~ /^$/) {shift @CES and next};
        
        # Parse each 'table' separately
        if ($CES[0] =~ /^Table/) {
            
            my $status = $csv->parse($CES[0]);
            my @fields = $csv->fields();
            my $table = $fields[1];
            
            # Create the CES table in the master struct
            push(@{$CES{Tables}},$table);
            push(@{$CES{Table}->{$table}}, shift @CES);
            
            while (@CES && $CES[0] !~ /^Table/) {
                push (@{$CES{Table}->{$table}}, shift @CES);
            };                

            next;
        }
        
        Error("Unexpected Line in $file:\n$CES[0]");

    }
    
    Dump(0,"Tables",\@{$CES{Tables}});

}


