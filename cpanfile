requires 'perl', '5.10.1';

requires 'Data::Dumper';
requires 'File::Slurp';
requires 'File::Basename';
requires 'File::Path';
requires 'List::Util';
requires 'Text::CSV';
requires 'Time::CTime';
requires 'Time::localtime';

